import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.*;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.awt.Image;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import java.awt.Toolkit;

public class StartGame extends JPanel implements Runnable, MouseMotionListener,
        MouseListener {

    Random generator = new Random();
    private int selectValue;
    private static final long serialVersionUID = 1L;
    private int bloop, counter = 0, playerY = 500, direction = -10, x = 55, y = 50, gunX, speed, bulletStop = playerY,
            alienfireX, alienfireY, playerX, playerWidth = 50,
            playerHeight = 30, scoreTracker = 0,  moveValue = 1, incrementValue = 0, livesTracker = 2,
            gameSpeed = 0, highScore = 0;
    private ArrayList<Invader> invaders;
    private boolean fired = false;
    private boolean hit = false;
    private boolean alienfire = false;
    private boolean gameOn = true;
    private Player newPlayer;
    private JLabel highScoreLabel = new JLabel();
    private JLabel highScoreValue = new JLabel();
    private JLabel score = new JLabel();
    private JLabel playerScore = new JLabel();
    private JLabel lives = new JLabel();
    private JLabel playerLives = new JLabel();
    private Image player, alien, death, bunker3, bunkerMiddle3, bunker2, bunkerMiddle2, bunker1, bunkerMiddle1;
    private JPanel [][] a,s,d = new JPanel[3][3];
    private int[] l = {3,3,3,3,3};
    private int[] r = {3,3,3,3,3};
    private int[] c = {3,3,3,3,3};
    
    public void setBunker(int [] array){
        array = new int[5];
        for (int i = 0; i<5; i++){
            array[i] = 3;
        }
    }

    public StartGame(int gameSpeed) {
        newPlayer = new Player();
        this.speed = gameSpeed;
        invaders = new ArrayList<Invader>();

        for (int k = 0; k < 5; k++) {
            // 50 Invaders!
            for (int i = 0; i < 9; i++) {
                Invader ainvader = new Invader(x, y);
                invaders.add(ainvader);
                x = x + 60; //changes spacing X of aliens
            }
            y = y + 35; //changes spacing Y of aliens
            x = 55;
        }
        Font scoreFont = new Font("Arial", 1, 20);
        highScoreLabel.setFont(scoreFont);
        highScoreValue.setFont(scoreFont);
        highScoreLabel.setForeground(Color.WHITE);
        highScoreValue.setForeground(Color.RED);
        highScoreLabel.setText("HIGH SCORE:");
        highScoreValue.setText(Integer.toString(highScore));
        score.setFont(scoreFont);
        score.setForeground(Color.WHITE);
        score.setText("SCORE:");
        playerScore.setFont(scoreFont);
        playerScore.setForeground(Color.RED);
        playerScore.setText(newPlayer.getScore());
        lives.setFont(scoreFont);
        lives.setForeground(Color.WHITE);
        lives.setText("Lives: ");
        playerLives.setFont(scoreFont);
        playerLives.setForeground(Color.BLUE);
        playerLives.setText(newPlayer.getLives());
        super.add(highScoreLabel);
        super.add(highScoreValue);
        super.add(score);
        super.add(playerScore);
        super.add(lives);
        super.add(playerLives);
        super.setBackground(Color.BLACK);
        super.addMouseMotionListener(this);
        super.addMouseListener(this);
        new Thread(this).start();
    }

    private void setDirection(int dirx) {
        this.direction = dirx;
    }

    public void mouseMoved(MouseEvent e) {
        if (gameOn) {
            if (e.getX() > 759) {
                playerX = 759;
            } else if (e.getX() < 20) {
                playerX = 20;
            } else {
                playerX = e.getX();
            }
        }
    }
    public void mouseDragged(MouseEvent e) {}
    public void mousePressed(MouseEvent e) {}
    public void mouseReleased(MouseEvent e) {}
    public void mouseEntered(MouseEvent e) {}
    public void mouseExited(MouseEvent e) {}
    public void mouseClicked(MouseEvent e) {
        if (gameOn) {
            if (fired == false) {
                gunX = e.getX();
                    if (gunX > 760) {
                        gunX = 760;
                    } else if (gunX < 21) {
                        gunX = 21;
                    } else {
                        gunX = e.getX();
                    }
                fired = true;
                hit = false;
            }
        }
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        if (gameOn) {
            drawPlayer(g);
            drawAlien(g);
            alienFire(g);
            drawBunker(g);
            //setBunker(l);
            //setBunker(r);
            //setBunker(c);
            if (fired == true) {
                startGun(g);
            }
        } 
        else {
            Font font = new Font("Arial", Font.PLAIN, 60);
            this.setBackground(Color.BLACK);
            g.setColor(Color.RED);
            g.setFont(font);
            g.drawString("GAME OVER", 255, 255);
        }
    }

    public void drawAlien(Graphics g) {
        Iterator<Invader> it = invaders.iterator();
        if (counter < 15){
            while (it.hasNext()) {
                Invader newinvader = (Invader) it.next();
                alien = Toolkit.getDefaultToolkit().getImage("invader.jpg");
                g.drawImage(alien, newinvader.getX()-21, newinvader.getY(), this);
            }
            counter++;
        }
        else if ((counter < 30)&&(counter >= 15)){
            while (it.hasNext()) {
                Invader newinvader = (Invader) it.next();
                alien = Toolkit.getDefaultToolkit().getImage("invader2.jpg");
                g.drawImage(alien, newinvader.getX()-21, newinvader.getY(), this);
            }
            counter++;
            if (counter >= 30){
                counter = 0;
            }
        }
    }

    public void drawPlayer(Graphics g) {
        player = Toolkit.getDefaultToolkit().getImage("player.jpg");
        g.drawImage(player, playerX-17, 500, this);
    }
    
    public void drawBunker(Graphics g){                
        g.setColor(Color.DARK_GRAY);
        g.fillRect(560, 400, 100, 50);
        g.setColor(Color.BLACK);
        g.fillRect(590, 425, 40,  25);
        
        // left bunker health at 3
        if (l[0] == 3){
            bunker3 = Toolkit.getDefaultToolkit().getImage("bunker.jpg");
            g.drawImage(bunker3, 120, 425, this);
        }
        if (l[1] == 3){
            bunker3 = Toolkit.getDefaultToolkit().getImage("bunker.jpg");
            g.drawImage(bunker3, 120, 400, this);
        }
        if (l[2] == 3){
            bunkerMiddle3 = Toolkit.getDefaultToolkit().getImage("bunkerMiddle.jpg");
            g.drawImage(bunkerMiddle3, 150, 400, this);
        }
        if (l[3] == 3){
            bunker3 = Toolkit.getDefaultToolkit().getImage("bunker.jpg");
            g.drawImage(bunker3, 190, 400, this);
        }
        if (l[4] == 3){
            bunker3 = Toolkit.getDefaultToolkit().getImage("bunker.jpg");
            g.drawImage(bunker3, 190, 425, this);
        }
               
        // left bunker health at 2
        if (l[0] == 2){
            bunker2 = Toolkit.getDefaultToolkit().getImage("bunker2.jpg");
            g.drawImage(bunker2, 120, 425, this);
        }
        if (l[1] == 2){
            bunker2 = Toolkit.getDefaultToolkit().getImage("bunker2.jpg");
            g.drawImage(bunker2, 120, 400, this);
        }
        if (l[2] == 2){
            bunkerMiddle2 = Toolkit.getDefaultToolkit().getImage("bunkerMiddle2.jpg");
            g.drawImage(bunkerMiddle2, 150, 400, this);
        }
        if (l[3] == 2){
            bunker2 = Toolkit.getDefaultToolkit().getImage("bunker2.jpg");
            g.drawImage(bunker2, 190, 400, this);
        }
        if (l[4] == 2){
            bunker2 = Toolkit.getDefaultToolkit().getImage("bunker2.jpg");
            g.drawImage(bunker2, 190, 425, this);
        }
        
        // left bunker health at 1
        if (l[0] == 1){
            bunker1 = Toolkit.getDefaultToolkit().getImage("bunker1.jpg");
            g.drawImage(bunker1, 120, 425, this);
        }
        if (l[1] == 1){
            bunker1 = Toolkit.getDefaultToolkit().getImage("bunker1.jpg");
            g.drawImage(bunker1, 120, 400, this);
        }
        if (l[2] == 1){
            bunkerMiddle1 = Toolkit.getDefaultToolkit().getImage("bunkerMiddle1.jpg");
            g.drawImage(bunkerMiddle1, 150, 400, this);
        }
        if (l[3] == 1){
            bunker1 = Toolkit.getDefaultToolkit().getImage("bunker1.jpg");
            g.drawImage(bunker1, 190, 400, this);
        }
        if (l[4] == 1){
            bunker1 = Toolkit.getDefaultToolkit().getImage("bunker1.jpg");
            g.drawImage(bunker1, 190, 425, this);
        }
        
        
        // left bunker health at 0
        if (l[0] <= 0){
            g.setColor(Color.BLACK);
            g.fillRect(120,425,30,26);
        }
        if (l[1] <= 0){
            g.setColor(Color.BLACK);
            g.fillRect(120,400,30,25);
        }
        if (l[2] <= 0){
            g.setColor(Color.BLACK);
            g.fillRect(150,400,40,25);
        }
        if (l[3] <= 0){
            g.setColor(Color.BLACK);
            g.fillRect(190,400,31,25);
        }
        if (l[4] <= 0){
            g.setColor(Color.BLACK);
            g.fillRect(190,425,31,26);
        }
        
        // center bunker health at 3
        if (c[0] == 3){
            bunker3 = Toolkit.getDefaultToolkit().getImage("bunker.jpg");
            g.drawImage(bunker3, 340, 425, this);
        }
        if (c[1] == 3){
            bunker3 = Toolkit.getDefaultToolkit().getImage("bunker.jpg");
            g.drawImage(bunker3, 340, 400, this);
        }
        if (c[2] == 3){
            bunkerMiddle3 = Toolkit.getDefaultToolkit().getImage("bunkerMiddle.jpg");
            g.drawImage(bunkerMiddle3, 370, 400, this);
        }
        if (c[3] == 3){
            bunker3 = Toolkit.getDefaultToolkit().getImage("bunker.jpg");
            g.drawImage(bunker3, 410, 400, this);
        }
        if (c[4] == 3){
            bunker3 = Toolkit.getDefaultToolkit().getImage("bunker.jpg");
            g.drawImage(bunker3, 410, 425, this);
        }
               
        // center bunker health at 2
        if (c[0] == 2){
            bunker2 = Toolkit.getDefaultToolkit().getImage("bunker2.jpg");
            g.drawImage(bunker2, 340, 425, this);
        }
        if (c[1] == 2){
            bunker2 = Toolkit.getDefaultToolkit().getImage("bunker2.jpg");
            g.drawImage(bunker2, 340, 400, this);
        }
        if (c[2] == 2){
            bunkerMiddle2 = Toolkit.getDefaultToolkit().getImage("bunkerMiddle2.jpg");
            g.drawImage(bunkerMiddle2, 370, 400, this);
        }
        if (c[3] == 2){
            bunker2 = Toolkit.getDefaultToolkit().getImage("bunker2.jpg");
            g.drawImage(bunker2, 410, 400, this);
        }
        if (c[4] == 2){
            bunker2 = Toolkit.getDefaultToolkit().getImage("bunker2.jpg");
            g.drawImage(bunker2, 410, 425, this);
        }
        
        // center bunker health at 1
        if (c[0] == 1){
            bunker1 = Toolkit.getDefaultToolkit().getImage("bunker1.jpg");
            g.drawImage(bunker1, 340, 425, this);
        }
        if (c[1] == 1){
            bunker1 = Toolkit.getDefaultToolkit().getImage("bunker1.jpg");
            g.drawImage(bunker1, 340, 400, this);
        }
        if (c[2] == 1){
            bunkerMiddle1 = Toolkit.getDefaultToolkit().getImage("bunkerMiddle1.jpg");
            g.drawImage(bunkerMiddle1, 370, 400, this);
        }
        if (c[3] == 1){
            bunker1 = Toolkit.getDefaultToolkit().getImage("bunker1.jpg");
            g.drawImage(bunker1, 410, 400, this);
        }
        if (c[4] == 1){
            bunker1 = Toolkit.getDefaultToolkit().getImage("bunker1.jpg");
            g.drawImage(bunker1, 410, 425, this);
        }
        // center bunker health at 0
        if (c[0] <= 0){
            g.setColor(Color.BLACK);
            g.fillRect(340,425,30,25);
        }
        if (c[1] <= 0){
            g.setColor(Color.BLACK);
            g.fillRect(340,400,30,25);
        }
        if (c[2] <= 0){
            g.setColor(Color.BLACK);
            g.fillRect(370,400,40,25);
        }
        if (c[3] <= 0){
            g.setColor(Color.BLACK);
            g.fillRect(410,400,31,25);
        }
        if (c[4] <= 0){
            g.setColor(Color.BLACK);
            g.fillRect(410,425,31,25);
        }
        
        // right bunker health at 3
        if (r[0] == 3){
            bunker3 = Toolkit.getDefaultToolkit().getImage("bunker.jpg");
            g.drawImage(bunker3, 560, 425, this);
        }
        if (r[1] == 3){
            bunker3 = Toolkit.getDefaultToolkit().getImage("bunker.jpg");
            g.drawImage(bunker3, 560, 400, this);
        }
        if (r[2] == 3){
            bunkerMiddle3 = Toolkit.getDefaultToolkit().getImage("bunkerMiddle.jpg");
            g.drawImage(bunkerMiddle3, 590, 400, this);
        }
        if (r[3] == 3){
            bunker3 = Toolkit.getDefaultToolkit().getImage("bunker.jpg");
            g.drawImage(bunker3, 630, 400, this);
        }
        if (r[4] == 3){
            bunker3 = Toolkit.getDefaultToolkit().getImage("bunker.jpg");
            g.drawImage(bunker3, 630, 425, this);
        }
               
        // right bunker health at 2
        if (r[0] == 2){
            bunker2 = Toolkit.getDefaultToolkit().getImage("bunker2.jpg");
            g.drawImage(bunker2, 560, 425, this);
        }
        if (r[1] == 2){
            bunker2 = Toolkit.getDefaultToolkit().getImage("bunker2.jpg");
            g.drawImage(bunker2, 560, 400, this);
        }
        if (r[2] == 2){
            bunkerMiddle2 = Toolkit.getDefaultToolkit().getImage("bunkerMiddle2.jpg");
            g.drawImage(bunkerMiddle2, 590, 400, this);
        }
        if (r[3] == 2){
            bunker2 = Toolkit.getDefaultToolkit().getImage("bunker2.jpg");
            g.drawImage(bunker2, 630, 400, this);
        }
        if (r[4] == 2){
            bunker2 = Toolkit.getDefaultToolkit().getImage("bunker2.jpg");
            g.drawImage(bunker2, 630, 425, this);
        }
        
        // right bunker health at 1
        if (r[0] == 1){
            bunker1 = Toolkit.getDefaultToolkit().getImage("bunker1.jpg");
            g.drawImage(bunker1, 560, 425, this);
        }
        if (r[1] == 1){
            bunker1 = Toolkit.getDefaultToolkit().getImage("bunker1.jpg");
            g.drawImage(bunker1, 560, 400, this);
        }
        if (r[2] == 1){
            bunkerMiddle1 = Toolkit.getDefaultToolkit().getImage("bunkerMiddle1.jpg");
            g.drawImage(bunkerMiddle1, 590, 400, this);
        }
        if (r[3] == 1){
            bunker1 = Toolkit.getDefaultToolkit().getImage("bunker1.jpg");
            g.drawImage(bunker1, 630, 400, this);
        }
        if (r[4] == 1){
            bunker1 = Toolkit.getDefaultToolkit().getImage("bunker1.jpg");
            g.drawImage(bunker1, 630, 425, this);
        }

        // right bunker health at 0
        if (r[0] <= 0){
            g.setColor(Color.BLACK);
            g.fillRect(560,425,30,25);
        }
        if (r[1] <= 0){
            g.setColor(Color.BLACK);
            g.fillRect(560,400,30,25);
        }
        if (r[2] <= 0){
            g.setColor(Color.BLACK);
            g.fillRect(590,400,40,25);
        }
        if (r[3] <= 0){
            g.setColor(Color.BLACK);
            g.fillRect(630,400,31,25);
        }
        if (r[4] <= 0){
            g.setColor(Color.BLACK);
            g.fillRect(630,425,31,25);
        }
    }

    public void startGun(Graphics g) {
        Iterator<Invader> it = invaders.iterator();
        while (it.hasNext() && fired == true && hit == false) {
            Invader newinvader = (Invader) it.next();
            // FOR BUNKERS LEFT(might be necessary to put it into another function)
            if(l[0] > 0){
                if ((gunX > 120)&&(gunX<=150)&&(bulletStop>400)&&(bulletStop<=426)){
                    removeBullet(g);
                    l[0]--;
                }
            }
            if(l[1] > 0){
                if ((gunX > 120)&&(gunX<=150)&&(bulletStop>375)&&(bulletStop<=400)){
                    removeBullet(g);
                    l[1]--;
                }
            }
            if(l[2] > 0){
                if ((gunX > 150)&&(gunX<=190)&&(bulletStop>375)&&(bulletStop<=400)){
                    removeBullet(g);
                    l[2]--;
                }
            }
            if(l[3] > 0){
                if ((gunX > 190)&&(gunX<=221)&&(bulletStop>375)&&(bulletStop<=400)){
                    removeBullet(g);
                    l[3]--;
                }
            }
            if(l[4] > 0){
                if ((gunX > 190)&&(gunX<=221)&&(bulletStop>400)&&(bulletStop<=426)){
                    removeBullet(g);
                    l[4]--;
                }
            }
            
            // FOR BUNKERS MIDDLE
            if(c[0] > 0){
                if ((gunX > 340)&&(gunX<=370)&&(bulletStop>400)&&(bulletStop<=425)){
                    removeBullet(g);
                    c[0]--;
                }
            }
            if(c[1] > 0){
                if ((gunX > 340)&&(gunX<=370)&&(bulletStop>375)&&(bulletStop<=400)){
                    removeBullet(g);
                    c[1]--;
                }
            }
            if(c[2] > 0){
                if ((gunX > 370)&&(gunX<=410)&&(bulletStop>375)&&(bulletStop<=400)){
                    removeBullet(g);
                    c[2]--;
                }
            }
            if(c[3] > 0){
                if ((gunX > 410)&&(gunX<=440)&&(bulletStop>375)&&(bulletStop<=400)){
                    removeBullet(g);
                    c[3]--;
                }
            }
            if(c[4] > 0){
                if ((gunX > 410)&&(gunX<=440)&&(bulletStop>400)&&(bulletStop<=425)){
                    removeBullet(g);
                    c[4]--;
                }
            }
            
            // FOR BUNKERS RIGHT
            if(r[0] > 0){
                if ((gunX > 560)&&(gunX<=590)&&(bulletStop>400)&&(bulletStop<=425)){
                    removeBullet(g);
                    r[0]--;
                }
            }
            if(r[1] > 0){
                if ((gunX > 560)&&(gunX<=590)&&(bulletStop>375)&&(bulletStop<=400)){
                    removeBullet(g);
                    r[1]--;
                }
            }
            if(r[2] > 0){
                if ((gunX > 590)&&(gunX<=630)&&(bulletStop>375)&&(bulletStop<=400)){
                    removeBullet(g);
                    r[2]--;
                }
            }
            if(r[3] > 0){
                if ((gunX > 630)&&(gunX<=660)&&(bulletStop>375)&&(bulletStop<=400)){
                    removeBullet(g);
                    r[3]--;
                }
            }
            if(r[4] > 0){
                if ((gunX > 630)&&(gunX<=660)&&(bulletStop>375)&&(bulletStop<=425)){
                    removeBullet(g);
                    r[4]--;
                }
            }
            if (((newinvader.getX()+21) > gunX) && ((newinvader.getX()-21) < gunX) && ((newinvader.getY()+15) > bulletStop) && ((newinvader.getY()-15) < bulletStop)) {
                if(incrementValue > 3) {
                    moveValue = moveValue+1;
                    incrementValue = 0;
                }
                else {
                    incrementValue++;
                }
                it.remove();
                removeBullet(g);
                checkStatus();
                // 32 represents the number of aliens
                scoreTracker = scoreTracker + (this.speed * 2)
                        + (32 - invaders.size()) + 1;
                scoreTracker++;
                newPlayer.setScore(scoreTracker);
                newPlayer.setLives(livesTracker);
            }
        }
        if ((fired == true) && (hit == false)) {
            g.setColor(Color.YELLOW);
            g.fillRect(gunX, bulletStop, 2, 7);
            bulletStop -= 25;
            if (bulletStop < 50) {
                removeBullet(g);
            }
        }
    }

    public void checkStatus() {
        Iterator<Invader> it = invaders.iterator();

        if (!it.hasNext()) {
            gameOver();
        }
    }

    public void removeBullet(Graphics g) {
        g.dispose();
        hit = true;
        fired = false;
        bulletStop = playerY;
    }

    public int moveDownRight() {
        Iterator<Invader> firstIterator = invaders.iterator();

        while (firstIterator.hasNext()) {
            Invader invader1 = (Invader) firstIterator.next();
            if (invader1.getY() > 499) {
                gameOver();
            }
            invader1.setY(invader1.getY() + 1);
        }
        return (moveValue*-1);
    }

    public int moveDownLeft() {
        Iterator<Invader> secondIterator = invaders.iterator();
        while (secondIterator.hasNext()) {
            Invader invader2 = (Invader) secondIterator.next();
            if (invader2.getY() > 499) {
                gameOver();
            }
            invader2.setY(invader2.getY() + 1);
        }
        return (moveValue);
    }

    public void alienFire(Graphics g) {
        if (alienfire == true) {
            if (alienfireY < 600) {
                if ((alienfireY < (playerY + 17))
                        && (alienfireY > (playerY - 17))
                        && (alienfireX > (playerX - 17))
                        && (alienfireX < (playerX + 17))) {
                    livesTracker -= 1;
                    if (livesTracker == -1){
                        gameOver();
                    }
                    alienfire = false;
                }
                
                // FOR BUNKERS LEFT
                if(l[0] > 0){
                    if ((alienfireX > 120)&&(alienfireX<=150)&&(alienfireY>425)&&(alienfireY<=451)){
                        alienfire = false;
                        l[0]--;
                    }
                }
                if(l[1] > 0){
                    if ((alienfireX > 120)&&(alienfireX<=150)&&(alienfireY>400)&&(alienfireY<=425)){
                        alienfire = false;
                        l[1]--;
                    }
                }
                if(l[2] > 0){
                    if ((alienfireX > 150)&&(alienfireX<=190)&&(alienfireY>400)&&(alienfireY<=425)){
                        alienfire = false;
                        l[2]--;
                    }
                }
                if(l[3] > 0){
                    if ((alienfireX > 190)&&(alienfireX<=221)&&(alienfireY>400)&&(alienfireY<=425)){
                        alienfire = false;
                        l[3]--;
                    }
                }
                if(l[4] > 0){
                    if ((alienfireX > 190)&&(alienfireX<=221)&&(alienfireY>425)&&(alienfireY<=451)){
                        alienfire = false;
                        l[4]--;
                    }
                }
                
                // FOR BUNKERS MIDDLE
                if(c[0] > 0){
                    if ((alienfireX > 340)&&(alienfireX<=370)&&(alienfireY>425)&&(alienfireY<=450)){
                        alienfire = false;
                        c[0]--;
                    }
                }
                if(c[1] > 0){
                    if ((alienfireX > 340)&&(alienfireX<=370)&&(alienfireY>400)&&(alienfireY<=425)){
                        alienfire = false;
                        c[1]--;
                    }   
                }
                if(c[2] > 0){
                    if ((alienfireX > 370)&&(alienfireX<=410)&&(alienfireY>400)&&(alienfireY<=425)){
                        alienfire = false;
                        c[2]--;
                        }
                }
                if(c[3] > 0){
                    if ((alienfireX > 410)&&(alienfireX<=440)&&(alienfireY>400)&&(alienfireY<=425)){
                        alienfire = false;
                        c[3]--;
                    }
                }
                if(c[4] > 0){
                    if ((alienfireX > 410)&&(alienfireX<=440)&&(alienfireY>425)&&(alienfireY<=450)){
                        alienfire = false;
                        c[4]--;
                    }
                }
                
                // FOR BUNKERS RIGHT
                if(r[0] > 0){
                    if ((alienfireX > 560)&&(alienfireX<=590)&&(alienfireY>425)&&(alienfireY<=450)){
                        alienfire = false;
                        r[0]--;
                    }
                }
                if(r[1] > 0){
                    if ((alienfireX > 560)&&(alienfireX<=590)&&(alienfireY>400)&&(alienfireY<=425)){
                        alienfire = false;
                        r[1]--;
                    }
                }
                if(r[2] > 0){
                    if ((alienfireX > 590)&&(alienfireX<=630)&&(alienfireY>400)&&(alienfireY<=425)){
                        alienfire = false;
                        r[2]--;
                    }
                }
                if(r[3] > 0){
                    if ((alienfireX > 630)&&(alienfireX<=660)&&(alienfireY>400)&&(alienfireY<=425)){
                        alienfire = false;
                        r[3]--;
                    }
                }                
                if(r[4] > 0){
                    if ((alienfireX > 630)&&(alienfireX<=660)&&(alienfireY>425)&&(alienfireY<=450)){
                        alienfire = false;
                        r[4]--;
                    }
                }
                g.setColor(Color.WHITE);
                g.fillRect(alienfireX, alienfireY, 4, 8);
                alienfireY = alienfireY + 8;
                
            } 
            else {
                alienfire = false;
            }
        } 
        else{
            Iterator<Invader> it1 = invaders.iterator();
            while (it1.hasNext()) {
                Invader invader1 = (Invader) it1.next();
                bloop = invader1.getValue();
                if (selectValue == invader1.getValue()) {
                    alienfire = true;
                    alienfireX = invader1.getX();
                    alienfireY = invader1.getY();
                    alienFire(g);
                }
            }
        }
    }

    public void moveAliens() {
        Iterator<Invader> it1 = invaders.iterator();
        while (it1.hasNext()) {
            Invader invader1 = (Invader) it1.next();
            int x = invader1.getX();

            if (x >= 770 - 30 && direction != -10) {
                setDirection(moveDownRight());
            }

            if (x <= 30 && direction != 10) {
                setDirection(moveDownLeft());
            }
        }
        Iterator<Invader> it = invaders.iterator();
        while (it.hasNext()) {
            Invader invader3 = (Invader) it.next();
            if (invader3.isVisible()) {
                invader3.act(direction);
            }
        }
    }

    public void gameOver() {
        gameOn = false;
        this.repaint();
    }

    public void run() {
    	File highScoreFile = new File("highScore.txt");
    	Scanner s;
		try {
			s = new Scanner(highScoreFile);
			highScore = s.nextInt();
			s.close();
		} catch (FileNotFoundException e) {
			try {
				highScoreFile.createNewFile();
				highScore = 0;
			} catch (IOException e1) {
				JOptionPane.showMessageDialog(null, "Critical File Error");
				System.exit(1);
			}
		} catch (NoSuchElementException e){
			try {
				highScoreFile.createNewFile();
				highScore = 0;
			} catch (IOException e1) {
				JOptionPane.showMessageDialog(null, "Critical File Error");
				System.exit(1);
			}
		}

        while (gameOn) {
            selectValue = generator.nextInt(2500);
            this.repaint();
            moveAliens();
            highScoreValue.setText(Integer.toString(highScore));
            newPlayer.setScore(scoreTracker);
            playerScore.setText(newPlayer.getScore());
            newPlayer.setLives(livesTracker);
            playerLives.setText(newPlayer.getLives());
            try {
                Thread.sleep(this.speed);
            } catch (InterruptedException ex) {
                Logger.getLogger(StartGame.class.getName()).log(Level.SEVERE,
                        null, ex);
            }
        }
        
        if (newPlayer.getScoreI() > highScore){
        	highScore = newPlayer.getScoreI();
        	try {
    			FileWriter fw = new FileWriter(highScoreFile);
    			fw.write(Integer.toString(highScore));
    			fw.close();
    		} catch (IOException e) {
    			e.printStackTrace();
    			JOptionPane.showMessageDialog(null, "High score save error.");
    		}
        	
        }
        
        
    }
}
