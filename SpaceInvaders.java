 


import java.awt.Dimension;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

/**
 *
 * @author Archangel
 */
public class SpaceInvaders {

    private static int speed = 50; // Set the speed of the game

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                JMenuItem restartLevel, quitGame;
                restartLevel = new JMenuItem("Restart Level");
                quitGame = new JMenuItem("Quit Game");
                JFrame gameFrame = new JFrame("Space Invaders");
                JMenuBar menubar = new JMenuBar();
                JMenu gameControl = new JMenu("File");
                menubar.add(gameControl);
                gameControl.add(restartLevel);
                gameControl.add(quitGame);
                JMenu levelControl = new JMenu("Level Control");
                menubar.add(levelControl);
                JMenu sound = new JMenu("Sound");
                menubar.add(sound);
                gameFrame.setJMenuBar(menubar);
                restartLevel.addActionListener(new restartLevelAction());
                quitGame.addActionListener(new quitGameAction());
                gameFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                gameFrame.setPreferredSize(new Dimension(800, 600));
                gameFrame.getContentPane().add(new StartGame(speed));
                gameFrame.pack();
                gameFrame.setVisible(true);

            }
        });
    }
}
