 

public class Player extends Entity {
    private int score = 0;
    private int lives = 3;
    
    protected void setScore(int x) {
        this.score = x;
    }
    
    protected String getScore() {
        return Integer.toString(this.score);
    }
    
    protected int getScoreI(){
    	return this.score;
    }
    
    protected void setLives(int x){
        this.lives = x;
    }
    
    protected String getLives(){
        return Integer.toString(this.lives);
    }

}
